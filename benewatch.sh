#/bin/bash
# Benewatch
# 2018-2019 | "headless_cyborg"
# Version: 0.3
#####################################################################
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.
#####################################################################

# INTERNAL VARIABLES
source config
language=0 # english = 0; czech = 1
language_flag=0

# COLOR DEFINITIONS
cl_clear="\e[0m"
cl_lightgreen="\e[42m"
cl_red="\e[41m"

function monitoring ()
  {
    if ping -c 1 ${address} &> /dev/null
      then
        echo "$ping_ok"
      else
        echo "$ping_warning" | mail -s "$benewatch" $recipient
      fi
    temperature=$(curl --silent --user $username:$password http://$address/HMI00001.cgi | tac | tac | sed '115q;d' | sed 's/[^0-9]*//g')
    if [ "$temperature" -gt "$monitored_temperature" ]
      then
        echo -e "$cl_red$temp_too_high$cl_clear"
        echo "$temp_too_high_mail - $temperature °C" | mail -s "$boiler_temp" $recipient
        echo "$sending_emails"
        echo "$string_waiting_time"
          sleep $time
        echo "$string_starting_monitoring"
          sleep 1
          monitoring
    fi
    echo -e "$cl_lightgreen$string_temp_not_exceeded1 - $temperature $string_temp_not_exceeded2$cl_clear"
    echo "$string_waiting_time_next"
      sleep $time
    echo "$string_starting_monitoring"
      sleep 1
      monitoring
    }

# Use '-l 0' for English (default) and '-l 1' for Czech
if [ $# -gt 0 ]; then
  while getopts hl: name
    do
      case $name in
        h) echo "usage: $0 [-h] [-l]"; exit ;;
        l)  language_flag=1
            language="$OPTARG" ;;
        ?) echo "error: option -$OPTARG is not implemented"; exit 2 ;;
      esac
      done
      if [ ! -z "$language_flag" ]; then
        if [ $language -eq 1 ]; then
          echo "[BENEWATCH] Language set to Czech"
            source lang_czech
        else
          echo "[BENEWATCH] Language set to English"
            source lang_english
      fi
    fi
      shift $(($OPTIND - 1))
else
    echo "[BENEWATCH] No arguments, language set to English"
    language=0
    source lang_english
fi

echo "$string_monitoring_started"
  sleep 1
echo "$string_treshold"
  sleep 1
echo "$string_starting_monitoring"
  sleep 1
  monitoring
